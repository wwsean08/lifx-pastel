FROM python:3
ENTRYPOINT ["python3", "main.py"]
# Forces python to output even when not attached to a tty for kubernetes
# and headless docker usage
ENV PYTHONUNBUFFERED=0
WORKDIR tmp
COPY requirements.txt ./
RUN pip install -r requirements.txt
WORKDIR /app
COPY main.py main.py