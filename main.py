#!/usr/bin/env python3
# coding=utf-8

import sys
import datetime
import random
from time import sleep

from lifxlan import LifxLAN, utils, WorkflowException


# Get a pastel color hue, saturation, and temperature (hsk)
def get_color():
    rgb_colors = [(85, 239, 196), (129, 236, 236), (116, 185, 255), (162, 155, 254), (223, 230, 233),
                  (0, 184, 148), (0, 206, 201), (9, 132, 227), (108, 92, 231), (178, 190, 195),
                  (255, 234, 167), (250, 177, 160), (255, 118, 117), (253, 121, 168), (99, 110, 114),
                  (253, 203, 110), (225, 112, 85), (214, 48, 49), (232, 67, 147), (45, 52, 54)]
    return utils.RGBtoHSBK(random.choice(rgb_colors))


def get_turn_off_time():
    off_time_start = datetime.datetime.now()
    off_time_end = datetime.datetime.now()
    # From Midnight to 8am in pacific time
    return off_time_start.replace(hour=8, minute=0, second=0, microsecond=0), off_time_end.replace(hour=16, minute=0, second=0, microsecond=0)


# If it is after a certain time, then it should be turned off
def should_turn_off():
    now = datetime.datetime.now()
    off_start, off_end = get_turn_off_time()
    return off_start < now < off_end


# Main function
def main():
    num_lights = None
    if len(sys.argv) != 2:
        print(
            "\nDiscovery will go much faster if you provide the number of lights on your LAN:")
        print("  python {} <number of lights on LAN>\n".format(sys.argv[0]))
    else:
        num_lights = int(sys.argv[1])

    # instantiate LifxLAN client, num_lights may be None (unknown).
    # In fact, you don't need to provide LifxLAN with the number of bulbs at all.
    # lifx = LifxLAN() works just as well. Knowing the number of bulbs in advance
    # simply makes initial bulb discovery faster.
    print("Discovering lights...")
    lifx = LifxLAN(num_lights, False)
    # get devices
    multizone_lights = lifx.get_multizone_lights()

    if len(multizone_lights) > 0:
        strip = multizone_lights[0]
        print("Selecting " + strip.get_label())
        all_zones = strip.get_color_zones()
        original_zones = all_zones
        bright_zones = []

        try:
            while True:
                try:
                    if should_turn_off():
                        strip.set_power(False)
                        # Sleep for a minute
                        sleep(60)
                        continue
                    else:
                        print("Turning on the lights")
                        strip.set_power(True)
                    print("Sending colors")
                    bright_zones.clear()
                    for _ in all_zones:
                        h, s, _, k = get_color()
                        bright_zones.append((h, s, 65535, k))
                    strip.set_zone_colors(bright_zones, 5000, True)
                    sleep(5)
                except WorkflowException:
                    print("Ignoring workflow exception, it probably should resolve itself")
        except KeyboardInterrupt:
            strip.set_zone_colors(original_zones, 1000, True)
    else:
        print("No lights with MultiZone capability detected.")


if __name__ == "__main__":
    # execute only if run as a script
    main()
